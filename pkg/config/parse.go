package config

import (
	"fmt"

	corev1 "k8s.io/api/core/v1"
	rbacv1beta1 "k8s.io/api/rbac/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
)

var scheme = runtime.NewScheme()
var codecs = serializer.NewCodecFactory(scheme)

func init() {
	addToScheme(scheme)
}

func addToScheme(scheme *runtime.Scheme) {
	utilruntime.Must(corev1.AddToScheme(scheme))
	utilruntime.Must(rbacv1beta1.AddToScheme(scheme))
}

func CreateOperatorIntraFromConfig(path string) error {

	obj, groupVersionKind, err := parseMuli(path)
	if err != nil {
		return err
	}

	// now use switch over the type of the object
	// and match each type-case
	switch o := obj.(type) {
	case *corev1.Pod:
		fmt.Printf("%+v %+v\n", groupVersionKind, o)
	case *rbacv1beta1.Role:
		fmt.Printf("%+v %+v\n", groupVersionKind, o)
	case *rbacv1beta1.RoleBinding:
		fmt.Printf("%+v %+v\n", groupVersionKind, o)
	case *rbacv1beta1.ClusterRole:
		fmt.Printf("%+v %+v\n", groupVersionKind, o)
	case *rbacv1beta1.ClusterRoleBinding:
		fmt.Printf("%+v %+v\n", groupVersionKind, o)
	case *corev1.ServiceAccount:
		fmt.Printf("%+v %+v\n", groupVersionKind, o)
	default:
		fmt.Printf("%+v %+v\n", groupVersionKind, o)
	}

	return nil
}

func parseMuli(path string) (runtime.Object, *schema.GroupVersionKind, error) {
	decode := codecs.UniversalDeserializer().Decode
	obj, groupVersionKind, err := decode([]byte(path), nil, nil)

	if err != nil {
		return nil, nil, fmt.Errorf("error while decoding YAML object: %v", err)
	}

	return obj, groupVersionKind, err
}
