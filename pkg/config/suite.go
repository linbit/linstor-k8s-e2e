package config

import piraeusv1alpha1 "github.com/piraeusdatastore/piraeus-operator/pkg/apis/piraeus/v1alpha1"

// Suite is the configuration for a series of Test runs.
type Suite struct {
	// Tests is a series of tests configurations to run.
	Tests []*Test
	Settings
}

// Test is the configuration for an series of e2e tests using the same configuration.
type Test struct {
	piraeusv1alpha1.PiraeusNodeSetSpec

	Settings
}

// Settings hold general configuration values for tests.
type Settings struct {
	// Kubeconfig is the path to the kubeconfig of a running kubernetes cluster.
	Kubeconfig string
	// Namespace to run components in.
	Namespace string
}
