package client

import (
	"time"

	piraeusv1alpha1 "github.com/piraeusdatastore/piraeus-operator/pkg/apis/piraeus/v1alpha1"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	watch "k8s.io/apimachinery/pkg/watch"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd/api"
)

const (
	controllerSetsResourceName = "piraeuscontrollersets"
	nodeSetsResourceName       = "piraeusnodesets"
)

type ControllerGetter interface {
	ControllerSets(namespace string) ControllerSetsInterface
}

type NodeGetter interface {
	NodeSets(namespace string) NodeSetsInterface
}

type PiraeusClient struct {
	restClient rest.Interface
}

type controllerSets struct {
	restClient rest.Interface
	ns         string
}

type nodeSets struct {
	restClient rest.Interface
	ns         string
}

func (c *PiraeusClient) ControllerSets(ns string) ControllerSetsInterface {
	return &controllerSets{c.restClient, ns}
}

func (c *PiraeusClient) NodeSets(ns string) NodeSetsInterface {
	return &nodeSets{c.restClient, ns}
}

func NewForConfig(c *rest.Config) (*PiraeusClient, error) {
	config := *c
	config.ContentConfig.GroupVersion = &api.SchemeGroupVersion
	config.APIPath = "/apis"
	config.NegotiatedSerializer = serializer.DirectCodecFactory{CodecFactory: scheme.Codecs}
	config.UserAgent = rest.DefaultKubernetesUserAgent()

	client, err := rest.RESTClientFor(&config)
	if err != nil {
		return nil, err
	}

	return &PiraeusClient{restClient: client}, nil
}

type ControllerSetsInterface interface {
	Create(*piraeusv1alpha1.PiraeusControllerSet) (*piraeusv1alpha1.PiraeusControllerSet, error)
	Get(name string, opts *metav1.GetOptions) (*piraeusv1alpha1.PiraeusControllerSet, error)
	Delete(name string, opts *metav1.DeleteOptions) error
	Watch(opts metav1.ListOptions) (watch.Interface, error)
}

func (c *controllerSets) Create(pcs *piraeusv1alpha1.PiraeusControllerSet) (*piraeusv1alpha1.PiraeusControllerSet, error) {
	result := piraeusv1alpha1.PiraeusControllerSet{}
	err := c.restClient.
		Post().
		Namespace(c.ns).
		Resource(controllerSetsResourceName).
		Body(pcs).
		Do().
		Into(&result)

	return &result, err
}

func (c *controllerSets) Get(name string, opts *metav1.GetOptions) (*piraeusv1alpha1.PiraeusControllerSet, error) {
	result := piraeusv1alpha1.PiraeusControllerSet{}
	err := c.restClient.
		Get().
		Namespace(c.ns).
		Resource(controllerSetsResourceName).
		Name(name).
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(&result)

	return &result, err
}

func (c *controllerSets) Delete(name string, opts *metav1.DeleteOptions) error {
	return c.restClient.
		Delete().
		Namespace(c.ns).
		Resource(controllerSetsResourceName).
		Name(name).
		Body(opts).
		Do().
		Error()
}

func (c *controllerSets) Watch(opts metav1.ListOptions) (watch.Interface, error) {
	var timeout time.Duration
	if opts.TimeoutSeconds != nil {
		timeout = time.Duration(*opts.TimeoutSeconds) * time.Second
	}
	opts.Watch = true
	return c.restClient.Get().
		Namespace(c.ns).
		Resource(controllerSetsResourceName).
		VersionedParams(&opts, scheme.ParameterCodec).
		Timeout(timeout).
		Watch()
}

type NodeSetsInterface interface {
	Create(*piraeusv1alpha1.PiraeusNodeSet) (*piraeusv1alpha1.PiraeusNodeSet, error)
	Get(name string, opts *metav1.GetOptions) (*piraeusv1alpha1.PiraeusNodeSet, error)
	Delete(name string, opts *metav1.DeleteOptions) error
	Watch(opts metav1.ListOptions) (watch.Interface, error)
}

func (n *nodeSets) Create(pns *piraeusv1alpha1.PiraeusNodeSet) (*piraeusv1alpha1.PiraeusNodeSet, error) {
	result := piraeusv1alpha1.PiraeusNodeSet{}
	err := n.restClient.
		Post().
		Namespace(n.ns).
		Resource(nodeSetsResourceName).
		Body(pns).
		Do().
		Into(&result)

	return &result, err
}

func (n *nodeSets) Get(name string, opts *metav1.GetOptions) (*piraeusv1alpha1.PiraeusNodeSet, error) {
	result := piraeusv1alpha1.PiraeusNodeSet{}
	err := n.restClient.
		Get().
		Namespace(n.ns).
		Resource(nodeSetsResourceName).
		Name(name).
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(&result)

	return &result, err
}

func (n *nodeSets) Delete(name string, opts *metav1.DeleteOptions) error {
	return n.restClient.
		Delete().
		Namespace(n.ns).
		Resource(nodeSetsResourceName).
		Name(name).
		Body(opts).
		Do().
		Error()
}

func (n *nodeSets) Watch(opts metav1.ListOptions) (watch.Interface, error) {
	var timeout time.Duration
	if opts.TimeoutSeconds != nil {
		timeout = time.Duration(*opts.TimeoutSeconds) * time.Second
	}
	opts.Watch = true
	return n.restClient.Get().
		Namespace(n.ns).
		Resource(nodeSetsResourceName).
		VersionedParams(&opts, scheme.ParameterCodec).
		Timeout(timeout).
		Watch()
}
