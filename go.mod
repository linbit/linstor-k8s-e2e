module github.com/LINBIT/linstor-k8s-e2e

go 1.12

require (
	github.com/piraeusdatastore/piraeus-operator v0.0.0-00010101000000-000000000000
	k8s.io/api v0.0.0-20190620084959-7cf5895f2711
	k8s.io/apimachinery v0.0.0-20190612205821-1799e75a0719
	k8s.io/client-go v12.0.0+incompatible
)

replace (
	github.com/piraeusdatastore/piraeus-operator => github.com/piraeusdatastore/piraeus-operator v0.0.2-0.20190828170431-3760a5f01563 // indirect
	k8s.io/clinet-go => k8s.io/client-go v0.0.0-20190620085101-78d2af792bab // indirect
)
